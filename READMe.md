Commit Details:

Commit Message1: ReStructured project and added api factory and comparator method that helps our file execution

1. Added all the dependencies in build.gradle that are required for building this framework and also restructured framework .
   Added Api factory class which has a GET method to fire the Api using a simple Rest Assured client.
   Added ComparatorRunnable class so that it can validate the 2 Jsons.

Commit Message2: Added unit test to ComparatorRunnableTest class to validate the Json

2. Added a ComparatorRunnableTest class which can be used to unit test the Comparator Runnable class.
   This Class can be used to test various Json inputs

Commit Message3: Added UrlRunnable class for parallel execution of Api s from text file

3. Added a UrlRunnable class which is used to read two text files line by line.
   The execution of these Api s are made in parallel using ExecutorService in Threads.
   In this class once we read all JSON's, we store the result in a list and call the comparatorRunnable class to display the results.

Commit Message4: Added UrlRunnable class and a Main class to test parallel execution of Api s from text file.

4. Added a Main class which to test the parallel execution
   Also added two text files with 10 Api s each , executed them , compared the results and displayed them

Commit Message5: Slight correction in ReadMe

5. Text correction in ReadMe file

Commit Message6: Added 1000 Api s in each text file and tested the same

6. Both TextFile1 and TextFile2 have 100 Api s each and have been tested
   The results are displayed in the desired format

Commit Message7: Added ApiSteps class and api.feature

7. Added Api steps class and api.feature so that the tests can be invoked without a main class
   It is also a pre set up to run Gradle commands from terminal

Commit Message8: Added Gradle support to run from commandLine

8.  Cleaned up the project and now after this commit the code can be run from below command,
    // gradle executeFeatures

Commit Message9: Updated ReadMe.md

9.  Updated ReadMe.md with Info


*************************************************Instructions To Execute The Project**********************************************

 1. Use the url: git clone https://bhareth1@bitbucket.org/bhareth1/gojek.git to clone the project
 2. Preferred IDE  is Intellej IDEA to load the project
 3. Project built on Java 1.8, and Gradle 4.5.1
 4. Once successfully cloned , please import the project to Intellej as Gradle
 5. Build the project to download the dependiencies used in the code
 6. Please install "Cucumber for java" which you can download from: Intellejidea->preference->plugins
 6. Run the test using the command : gradle executeFeatures
 7. The text files are kept in the IDE with 1000 Api s each line by line. In case You want to use your text file give the correct path of prefered text file in the code
