package actions;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jayway.restassured.response.Response;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


public class ComparatorRunnable implements Runnable {

    private final String url1;
    private final String url2;

    public static ObjectMapper mapper = new ObjectMapper();

    public boolean result = false;

    public ComparatorRunnable(String url1, String url2) {
        this.url1 = url1;
        this.url2 = url2;
    }

    public void run() {
        try {
            String body1 = getBody(url1);
            String body2 = getBody(url2);
            result = recursiveCompare(body1, body2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    boolean recursiveCompare(String body1, String body2) throws JsonProcessingException {
        JsonNode node1 = mapper.readTree(body1);
        JsonNode node2 = mapper.readTree(body2);
        return recursiveCompare(node1, node2);
    }

    private boolean recursiveCompare(JsonNode node1, JsonNode node2) {
        if (!node1.getClass().equals(node2.getClass())) {
            return false;
        }
        if (node1 instanceof ObjectNode) {

            System.out.println("SIZE IS ------ " + node1.size());
            System.out.println("SIZE IS ------ " + node2.size());

            if(node1.size()!=node2.size()){
                return false;
            }
            Iterator<Map.Entry<String, JsonNode>> iterator = node1.fields();
            // go through each key value pair of first object
            while (iterator.hasNext()) {
                Map.Entry<String, JsonNode> entry = iterator.next();
                // check that the key must also exist in second object
                if (!node2.has(entry.getKey())) {
                    return false;
                }
                // if key exists in both objects, recursively compare the values
                if (!recursiveCompare(entry.getValue(), node2.get(entry.getKey()))) {
                    return false;
                }
            }
        } else if (node1 instanceof ArrayNode) {
            ArrayNode an1 = (ArrayNode) node1;
            ArrayNode an2 = (ArrayNode) node2;
            // size of array mismatch
            if (an1.size() != an2.size()) {
                return false;
            }
            for (int i = 0; i < an1.size(); i++) {
                // recursively compare every array element
                if (!recursiveCompare(an1.get(i), an2.get(i))) {
                    return false;
                }
            }

        } else {
            return node1.asText().equals(node2.asText());
        }
        return true;
    }


    private String getBody(String input) {
        Response response = ApiFactory.getApiResponse(200, input);
        String body = response.prettyPrint().toString();
        return body;

    }

    public String getUrl1() {
        return url1;
    }

    public String getUrl2() {
        return url2;
    }

    public boolean isResult() {
        return result;
    }
}
