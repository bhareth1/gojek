package actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class ComparatorRunnableTests {

    ComparatorRunnable obj = new ComparatorRunnable("dummy", "dummy");

    @Test
    public void testOrderedJson() throws JsonProcessingException {
        assertTrue(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", "{\"a\":\"1\", \"b\":\"2\"}"));
    }

    @Test
    public void testUnorderedJson() throws JsonProcessingException {
        assertTrue(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", "{\"b\":\"2\", \"a\":\"1\"}"));
    }

    @Test
    public void testArray() throws JsonProcessingException {
        assertTrue(obj.recursiveCompare("[1, 2, 3, \"x\"]",
                "[1, 2, 3, \"x\"]"));
    }

    @Test
    public void testBody() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", "{\"b\":\"2\"}"));
    }

    @Test
    public void noBodyInSecondJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", ""));
    }

    @Test
    public void rightLengthNotEqualJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", "{\"a\":\"1\", \"b\":\"2\",\"c\":\"3\"}"));
    }

    @Test
    public void leftLengthNotEqualJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\",\"c\":\"3\"}", "{\"a\":\"1\", \"b\":\"2\"}"));
    }

    @Test
    public void faultyJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", "{\"b\":\"1\", \"a\":\"2\"}"));
    }

    @Test
    public void emptyJson() throws JsonProcessingException {
        assertTrue(obj.recursiveCompare("", ""));
    }

    @Test
    public void rightEmptyJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", ""));
    }

    @Test
    public void leftEmptyJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("", "{\"a\":\"1\", \"b\":\"2\"}"));
    }

    @Test
    public void testkeyValueJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", "{\"a\":\"1\", \"c\":\"2\"}"));
    }

    @Test
    public void testValueEmptyJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", "{\"a\":\"\"}"));
    }

    @Test
    public void testkeyJson() throws JsonProcessingException {
        assertFalse(obj.recursiveCompare("{\"a\":\"1\", \"b\":\"2\"}", "{\"\":\"1\"}"));
    }
}
