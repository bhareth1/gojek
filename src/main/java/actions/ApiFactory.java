package actions;


import com.jayway.restassured.response.Response;
import static com.jayway.restassured.RestAssured.given;


public class ApiFactory {

    public static Response getApiResponse(int responseCode, String url) {
        Response res = given().contentType("application/json").get(url);
        int status = res.getStatusCode();
        System.out.println("ACTUAL STATUS IS: " + status + "Response Body: "+ res.getBody().prettyPrint());
        if (status != responseCode) {
            System.out.println("Something went wrong..Error is.." + status);
            return null;
        }
        return res;
    }
}
