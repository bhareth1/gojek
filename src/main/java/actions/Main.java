package actions;

import java.nio.file.Paths;


public class Main {

    public static void main(String[] args) throws Exception {

        String file1="TextFile1";
        String file2="TextFile2";
        UrlRunnable comparator = new UrlRunnable(Paths.get(file1), Paths.get(file2));
        comparator.compare();

    }

}
