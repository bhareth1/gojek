package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class UrlRunnable {

    private final Path p1;
    private final Path p2;

    public UrlRunnable(Path p1, Path p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void compare() throws IOException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors() * 4);
        BufferedReader reader1 = Files.newBufferedReader(p1);
        BufferedReader reader2 = Files.newBufferedReader(p2);
        List<ComparatorRunnable> list = new ArrayList<>();
        while (true) {
            String url1 = reader1.readLine();
            String url2 = reader2.readLine();

            if (url1 == null || url2 == null) {
                break;
            }
            ComparatorRunnable comparatorRunnable = new ComparatorRunnable(url1, url2);
            executorService.submit(comparatorRunnable);
            list.add(comparatorRunnable);

        }

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.DAYS);
        for (ComparatorRunnable comparatorRunnable : list) {
            if (comparatorRunnable.isResult()) {
                System.out.println(String.format("%s is equal to %s",
                        comparatorRunnable.getUrl1(), comparatorRunnable.getUrl2()));
            } else {
                System.out.println(String.format("%s is not equal to %s",
                        comparatorRunnable.getUrl1(), comparatorRunnable.getUrl2()));

            }
        }


    }

}
