package com.gojek.teststructure;

import actions.UrlRunnable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class ApiSteps {

    Properties properties;


        @Given("^I start \"([^\"]*)\" test$")
        public void printName(String name) {
            System.out.println("--------TEST STARTED--------\n" + name);
        }


        @Then("^I read all the Apis from \"([^\"]*)\" and \"([^\"]*)\" line by line and fire all the APIs line by line and compare$")
        public void iReadAllTheApisFromAndLineByLineAndFireAllTheAPIsLineByLineAndCompare(String fileName1, String fileName2) throws Exception {


            UrlRunnable comparator = new UrlRunnable(Paths.get(fileName1), Paths.get(fileName2));
            comparator.compare();

        }

        @Given("^I stop \"([^\"]*)\" test$")
        public void iStopTest(String name)  {
            System.out.println("--------TEST ENDED--------\n" + name);
        }
    }

