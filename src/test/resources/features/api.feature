Feature: Api

  Scenario: Fire an Api from two files and compare whether they are equal or not and print the same
    Given I start "Fire an Api from two files and compare whether their JSON is equal or not and print the result" test
     Then I read all the Apis from "TextFile1" and "TextFile2" line by line and fire all the APIs line by line and compare
    Given I stop "Fire an Api from two files and compare whether their JSON is equal or not and print the result" test